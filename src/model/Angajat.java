package model;

public class Angajat {
	int idAngajat;
	String nume, prenume, cnp;
	
	public Angajat() {}
	
	public Angajat(int idAngajat, String nume, String prenume, String cnp) {
		super();
		this.idAngajat = idAngajat;
		this.nume = nume;
		this.prenume = prenume;
		this.cnp = cnp;
	}

	public int getIdAngajat() {
		return idAngajat;
	}

	public void setIdAngajat(int idAngajat) {
		this.idAngajat = idAngajat;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getPrenume() {
		return prenume;
	}

	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}

	public String getCnp() {
		return cnp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	@Override
	public String toString() {
		return "Angajat [idAngajat=" + idAngajat + ", nume=" + nume + ", prenume=" + prenume + "]";
	}
}
