package test;

import database.Database;
import database.DatabaseConnection;
import model.Angajat;
import util.AngajatUtil;

public class Main {
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	
	// Connection string
	static final String DB_URL = "jdbc:mysql://localhost/atelier_croitorie";
	static final String USER = "root";
	static final String PASS = "1q2w3e";

	public static void main(String[] args) {
		Database database = new Database(JDBC_DRIVER, DB_URL, USER, PASS);
		DatabaseConnection connection = new DatabaseConnection(database);
		AngajatUtil angajatUtil = new AngajatUtil(connection);
		
		for (Angajat a : angajatUtil.getAll()) {
			System.out.println(a);
		}
		
		Angajat Ion = new Angajat(1, "Ion", "Marin", "1234567890123");
		angajatUtil.insertAngajat(Ion);
	}
}
