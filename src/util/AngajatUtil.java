package util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import database.DatabaseConnection;
import model.Angajat;

public class AngajatUtil {
	private final DatabaseConnection connection;

	public AngajatUtil(DatabaseConnection connection) {
		super();
		this.connection = connection;
	}

	public DatabaseConnection getConnection() {
		return connection;
	}
	
	public List<Angajat> getAll() {
		connection.createConnection();
		
		List<Angajat> res = new ArrayList<>();
		
		String query = "SELECT * FROM Angajat";
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = connection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				Angajat a = new Angajat();
				a.setIdAngajat(rs.getInt("id_angajat"));
				a.setNume(rs.getString("nume"));
				a.setPrenume(rs.getString("prenume"));
				a.setCnp(rs.getString("cnp"));
				res.add(a);
			}
		} catch (SQLException e) {
			System.out.println("Query failed: " + e.getMessage());
		} finally {
			connection.closeConnection();
			try {
				rs.close();
				ps.close();
			} catch (SQLException e) {
				System.out.println("Fail. " + e.getMessage());
			}
			
		}
		return res;
	}
	
	public void insertAngajat(Angajat a) {
		connection.createConnection();
		
		String query = "INSERT INTO Angajat VALUES(?,?,?,?)";
		PreparedStatement ps = null;
		
		try {
			ps = connection.getConnection().prepareStatement(query);
			
			ps.setInt(1, a.getIdAngajat());
			ps.setString(2, a.getNume());
			ps.setString(3, a.getPrenume());
			ps.setString(4, a.getCnp());
			
			ps.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Error when querying: " + e.getMessage());
		} finally {
			connection.closeConnection();
			try {
				ps.close();
			} catch (SQLException e) {
				System.out.println("Error when closing ps: " + e.getMessage());
			}
		}
	}
}
