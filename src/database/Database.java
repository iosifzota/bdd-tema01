package database;

public class Database {
	private String jdbcDriver, databaseUrl, user, password;
	
	public Database(String jdbcDriver, String databaseUrl, String user, String password) {
		super();
		this.jdbcDriver = jdbcDriver;
		this.databaseUrl = databaseUrl;
		this.user = user;
		this.password = password;
	}

	public String getJdbcDriver() {
		return jdbcDriver;
	}

	public String getDatabaseUrl() {
		return databaseUrl;
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public String toString() {
		return "Database [jdbcDriver=" + jdbcDriver + ", databaseUrl=" + databaseUrl + ", user=" + user + ", password="
				+ password + "]";
	}
}
