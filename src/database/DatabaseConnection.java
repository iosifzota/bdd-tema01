package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
	private Database database;
	private Connection connection = null;

	public DatabaseConnection(Database database) {
		this.database = database;
	}

	public void createConnection() {
		registerJdbcDriver();
		openConnection();
	}

	private void registerJdbcDriver() {
		try {
			Class.forName(database.getJdbcDriver());
		} catch (ClassNotFoundException e) {
			System.out.println("Could not find class driver: " + e.getMessage());
		}
	}

	private void openConnection() {
		System.out.println("Connecting to database...");
		try {
			connection = DriverManager.getConnection(database.getDatabaseUrl(), database.getUser(),
					database.getPassword());
		} catch (SQLException e) {
			System.out.println("Could not find connectin: " + e.getMessage());
		}
	}
	
	public void closeConnection() {
		try {
			connection.close();
		} catch (SQLException e) {
			System.out.println("Failed to close connection: " + e.getMessage());
		}
	}
	
	public Database getDatabase() {
		return database;
	}

	public Connection getConnection() {
		return connection;
	}
}
